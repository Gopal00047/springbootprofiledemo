package com.example.demo;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ConfigurationProperties("spring.app")
public class ProfileConfiguration {

	private String name;
	private String profile;
	
	@Profile("dev")
	@Bean
	public String getDevProfile() {
		
		System.out.println("Dev profile name = "+name);
		
		return profile;
	}
	
	@Profile("prod")
	
	public String getProdProfile() {
		
		System.out.println("Prod profile name = "+name);
		
		return profile;
	}
}
